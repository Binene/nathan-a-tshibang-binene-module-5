import 'package:flutter/material.dart';
import 'package:flutter_module_5/main.dart';
import 'package:flutter_module_5/profile.dart';
import 'package:flutter_module_5/theme.dart';

class HomeScreem extends StatelessWidget {
  const HomeScreem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
        actions: [
          IconButton(
            icon: const Icon(Icons.brightness_4_rounded),
            onPressed: () {
              ///currentTheme.toggleTheme();
            },
          ),
        ],
      ),
      body: Column(
        children: [
          FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return profileed();
                  },
                ),
              );
            },
            child: Text("Profile"),
          ),
          SizedBox(
            height: 5,
          ),
          FloatingActionButton(
            onPressed: () {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return LoginScreen();
                  },
                ),
                (route) => false,
              );
            },
            child: Text("Sign\nOut"),
          ),
        ],
      ),
    );
  }
}
